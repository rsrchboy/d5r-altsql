FROM alpine:3.8
MAINTAINER Chris Weyl <cweyl@alumni.drew.edu>

RUN apk --no-cache add \
    build-base \
    perl-dev \
    perl-app-cpanminus \
    perl-config-any \
    perl-dbd-mysql \
    perl-dbd-pg \
    perl-dbd-sqlite \
    perl-extutils-config \
    perl-extutils-helpers \
    perl-extutils-installpaths \
    perl-json \
    perl-module-build \
    perl-module-build-tiny \
    perl-moose \
    perl-namespace-autoclean \
    perl-params-validate \
    perl-sub-identify \
    perl-term-readkey \
    perl-test-deep \
    perl-yaml \
    perl-module-pluggable \
    wget \
    && mkdir -p /root/.cpanm \
    && cpanm Devel::OverloadInfo Data::Structure::Util \
    && COLUMNS=80 LINES=80 cpanm Term::ReadLine::Zoid \
    && cpanm App::AltSQL \
    && rm -rf /root/.cpanm \
    && apk del build-base wget perl-dev

ENTRYPOINT [ "altsql" ]
